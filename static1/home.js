
/**Function**/

var num1 = 10;
num1 = num1 +1;
console.log(num1);

num1--;
console.log(num1);
console.log(num1%6);
//any number between//
num1 +=10;
console.log(num1);

/* Function
1. Create a function
2. Call the function
*/

function fun(){
    console.log('this is function');
}
//Call 

//fun();

/* Let`s create a function that take in a name and says hello followed by your name 
For example

Name: "Lucas"
Return: "Hello Lucas"
*/
function welcome(){
 var name = prompt('What is your name?');
 var result ='Hello '+ name; // String Concatenation 
console.log(result);
}

//welcome();


// How do arguments work in functions?

function sumNumbers(num1, num2) {
    var result = num1 + num2;
    console.log(num1 + num2);

}

sumNumbers(10,10)

console.log('CrashCourseJS')

//alert('Welcome')
/* Variables*/

var someNumber = 45;
console.log(someNumber);

//var age = prompt('What is your age?');

//document.getElementById('someText').innerHTML = age;


// While lops

var num1 = 0;

while (num1 < 100) {
    num1 +=1;
    console.log(num1);
}
// For loop iteration

for (let num1 = 0; num1 < 100; num1++) {
    console.log(num1);
}


// Data Types
let yourAge = 18; // number
let yourName = 'Lucas'; // string
let name = {firstName: 'Lucas', lastName: 'Cheps'}; //object
let truth = false; // boolean
let groceries = ['apple', 'banana', 'oranges']; // array
let random; //undefined
let nothing = null; // value null

// Strings in Javascript (common methods)
let fruit = 'banana';
let moreFruits = 'banana\apple';
console.log(fruit.length);
console.log(fruit.indexOf('apple'))
console.log(fruit.slice(2, 5));
console.log(fruit.replace('ban', '123'));
console.log(fruit.toUpperCase());
console.log(fruit.toLowerCase());
console.log(fruit.charAt(2));
console.log(fruit[2]);
console.log(fruit.split(',')); //split by a comma
console.log(fruit.split('')); //split by charters

//Array
let fruits = ['banana', 'apple', 'orange', 'pineapples'];
fruits = new Array('banana', 'apple', 'orange', 'pineapples');

//console.log(fruits[2]); //access value at index 2nd

//alert(fruits[2]);
fruits[0] = 'pear'; //change value of fruit at index 0
console.log(fruits);

for (let i = 0; i <fruit.length; i++);
console.log(fruits[0]);

//Array commons methods

console.log('toString', fruits.toString());
console.log(fruits.join(' * '));
console.log(fruits.pop(), fruits); // remove last item
console.log(fruits.push('blackberries'), fruits); // appends
console.log(fruits[4]);
fruits[fruits.length] = 'new fruit'; // same as push
console.log(fruits);
fruits.shift(); // remove first element from ana array
console.log(fruits)
fruits.unshift('kiwi'); // add first element to an array
console.log(fruits)

let vegetables = ['asparagus','tomato', 'broccoli'];
let allGroceries = fruits.concat(vegetables); // combine arrays
console.log(allGroceries);
console.log(allGroceries.slice(1, 4)); // 
console.log(allGroceries.reverse()); // flip items
console.log(allGroceries.sort()); 

let someNumbers = [5, 10, 2, 25, 3, 255, 1, 2, 5, 334, 321, 2];
console.log(someNumbers.sort()); // sort items
console.log(someNumbers.sort(function(a, b){return a-b})); // sorted in ascending order
console.log(someNumbers.sort(function(a, b){return b-a})); // sorted in descending order


let emptyArray =  new Array();
for (let  num = 0; num <= 10; num++) {
    emptyArray.push(num);
}
console.log(emptyArray);
// Objects in Javascript
// dictionary in Python

let student = {
    first:'Lucas',
    last:'Cheps',
    age: 38,
    height: 185,
    studentInfo: function (){
         return this.first + '\n ' + this.last + '\n ' + this.age;
     }
     };
//console.log(student.first);
//console.log(student.last);
//student.first =  'notLucas';  //change value
//console.log(student.first);
student.age++;
console.log(student.age);
console.log(student.studentInfo());

//Conditionals, Controls and flows (if else)


// 18-35 is my target demographic
// && AND 
//  || OR

var age  =  45;

if  (  (age>=18) && (age<=35)  )  {
    status  =  'target demo';
    console.log(status);
} else {
    status  =  ' not my audience';
   console.log(status);
}


// Switch statements

//  differentiate  between weekday vs weekend 
// day 0 --> Sunday --> weekend
// day 1 --> Monday --> weekday
// day 2 --> Tuesday --> weekday
// day 3 --> Wednesday --> weekday
//day 4 --> Thursday --> weekday 
// day 5 --> Friday --> weekday
// day  6 --> Saturday --> weekend

switch (2) {
    case  0:
    text  =  'weekend';
    break;
    case 5:
    text  =  'weekend';
    break;
    case 6:
    text  =  'weekend';
    break;
    default:
    text  =  'weekday';

}
console.log(text);
